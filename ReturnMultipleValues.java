class ReturnMultipleValues {
	
	public static void main(String[] args) {
		ScoreAndPlayer x = getWinner();
		System.out.println(x.score + " " + x.player);
	}
	
	static ScoreAndPlayer getWinner() {
		ScoreAndPlayer scoreAndPlayer = new ScoreAndPlayer();
		scoreAndPlayer.score = 0;
		scoreAndPlayer.player = "Player A";
		return scoreAndPlayer;
	}
	
}

class ScoreAndPlayer {
	int score;
	String player;
}